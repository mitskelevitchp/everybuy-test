import ItemType from "@/types/footerItemType";

export const footerItems: ItemType[] = [
  {
    src: "icons/house.svg",
    alt: "House",
    text: "Головна",
  },
  {
    src: "icons/smallHeart.svg",
    alt: "SmallHeart",
    text: "Вибране",
  },
  {
    src: "icons/addIcon.svg",
    alt: "AddIcon",
    text: "Створити",
  },
  {
    src: "icons/chat.svg",
    alt: "Chat",
    text: "Чат",
  },
  {
    src: "icons/smallUser.svg",
    alt: "SmallUser",
    text: "Профіль",
  },
];
