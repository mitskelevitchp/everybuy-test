import BannerItemType from "@/types/bannerType";

export const bannerItems: BannerItemType[] = [
  {
    label: "NEW",
    title: "Продам велосипед",
    condition: "вживане",
    price: "8490 грн",
    buttonName: "Купити",
    backgroundImage: "",
  },
  {
    label: "NEW",
    title: "Продам комплект посуду",
    condition: "нове",
    price: "1200 грн",
    buttonName: "Купити",
    backgroundImage: "",
  },
  {
    label: "SALE",
    title: "Розпродаж зимового одягу ",
    condition: "акція діє до кінця березня",
    price: "",
    buttonName: "Перейти",
    backgroundImage: "",
  },
  {
    label: "NEW",
    title: "Продам фотокамеру",
    condition: "вживане",
    price: "12450 грн",
    buttonName: "Купити",
    backgroundImage: "",
  },
  {
    label: "NEW",
    title: "Куплю офісне крісло",
    condition: "вживане/нове",
    price: "від 600 до 1400 грн",
    buttonName: "Запропонувати",
    backgroundImage: "",
  },
];
