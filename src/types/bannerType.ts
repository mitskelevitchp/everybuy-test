export default interface BannerItemType {
  label: string;
  title: string;
  condition: string;
  price: string;
  buttonName: string;
  backgroundImage: string;
}
