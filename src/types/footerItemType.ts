export default interface ItemType {
  src: string;
  alt: string;
  text: string;
}
